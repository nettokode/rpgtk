import tkinter as tk
from classes import Pers_rpg
from Profile_info import Profile_info


class MenuInicial():
    """ Uma classe para definir a tela inicial do RPG """
    def __init__(self,janela):
        self.janela = janela

        self.janela.geometry("700x600")

        self.janela.title("__RPG__")

        #Define os frames da janela
        self.frame1 = tk.Frame(self.janela)
        self.frame2 = tk.Frame(self.janela)
        self.frame3 = tk.Frame(self.janela)

        self.img = tk.PhotoImage(file="img.gif")
        
        #Epacota o label com a imagem do logo
        self.logo = tk.Label(self.frame1,image=self.img)
        self.logo.pack()

        #Pega as imformaçaoes do personagem
        self.lb_name = tk.Label(self.frame3,text="Name")
        self.lb_raça = tk.Label(self.frame3,text="Raça")
        self.lb_classe = tk.Label(self.frame3,text="Classe")

        self.entry_name = tk.Entry(self.frame3)
        self.entry_raça = tk.Entry(self.frame3)
        self.entry_classe = tk.Entry(self.frame3)

        self.lb_name.pack()
        self.entry_name.pack()
        self.lb_raça.pack()
        self.entry_raça.pack()
        self.lb_classe.pack()
        self.entry_classe.pack()

        self.bt_info = tk.Button(self.frame1,text="Exibir informaçoes")
        self.bt_valid = tk.Button(self.frame2,text="Confirmar")
        self.bt_valid.pack()
        self.bt_info.pack()
        self.bt_valid["command"] = self.make_pers
        self.bt_info["command"] = self.show_info

        #Epacota os frames da janela
        self.frame1.pack(side="top")
        self.frame2.pack(side="bottom")
        self.frame3.pack(side="bottom")
    

    def get_classe(self):
        self.classe = self.entry_classe.get()

    
    def get_name(self):
        self.name = self.entry_name.get()

    
    def get_raça(self):
        self.raça = self.entry_raça.get()
    
          
    def show_info(self):
        p_info = Profile_info(self.raça,self.classe,self.nome)


    def get_all(self):
        self.get_name()
        self.get_raça()
        self.get_classe()


    def make_pers(self):
        self.get_all()
        classe = Pers_rpg(self.classe,self.name,self.raça)
        