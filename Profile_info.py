import tkinter as tk

class Profile_info():
    def __init__(self,raça,classe,nome):
        self.raça = raça
        self.classe = classe
        self.nome = nome

        self.janela = tk.Tk()
        self.janela.title("Informaçoes do perssonagem")
        self.janela.geometry("500x400")

        self.frame1 = tk.Frame()
        self.frame2 = tk.Frame()
        self.frame3 = tk.Frame()
        
        self.lb_nome = tk.Label(self.frame1,text="Nome: %s"%self.nome)
        self.lb_classe = tk.Label(self.frame2)
        self.lb_raça = tk.Label(self.frame3)

        self.frame1.pack()
        self.frame2.pack()
        self.frame3.pack()

        if self.raça == "elfo":
            self.lb_raça["text"] = ("""Seres habitantes de regiões naturais,
                                       possuem uma feição com a natureza incompreendida por qualquer outra raça.
                                       Possuem orelhas ponte-agudas, suas vestes são geralmente verdes.
                                       Tem um dom de poder de comunicar com a natureza(incluindo animais) com apenas um toque,
                                       podendo com isso, ter conhecimento do que se trata o assunto abordado por tal ser natural.
                                       Possuem uma inteligência incrível, a melhor dentre as raças Terrenas.""")

        if self.raça == "humano":
            self.lb_raça["text"] = ("""Seres principais do Planeta Terra.
                                       São geralmente gananciosos,imaturos e incompetentes,
                                       embora, muito honesto e seguem sua palavra ate o fim.
                                       São dotados de uma incrível força, que também é sua destruição,
                                       se chama "Esperança", que lhe da forças de sempre acreditar no que quiser,
                                       e poder morrer por isso.""")

        if self.raça == "anão":
            self.lb_raça["text"] = ("""Seres que habitam regiões pedregosas, escondidas em cavernas.
                                       Sua estrutura óssea é realmente pequena e afofada, possuem geralmente barba.
                                       Possuem um pavio curto, no entanto, são muito hospitaleiros com outros seres.
                                       São os melhores na arte da refinaria de materiais metálicos(Ferreiros), alem de ótimos mineradores.
                                       Sua inteligência é considera a pior existente, pois,
                                       são seres brutos, preferem guerrear ao conversar.""")
        self.janela.mainloop()