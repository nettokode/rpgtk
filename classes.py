class Pers_rpg():
    def __init__(self,classe,nome,raça):
        self.classe = classe
        self.nome = nome
        self.raça = raça

        if self.raça.lower() == "elfo":
            self.elfo = {
                "nome":self.nome,"raça":"elfo","classe":self.classe,"atributos":{
                    "força":1,"agilidade":4,"magia":3
                    }
                }
            return self.elfo

        if self.raça.lower() == "humano":
            self.humano = {
                "nome":"","raça":"humano","classe":"","atributos":{
                    "força":3,"agilidade":3,"magia":2
                    }
                }
            return self.humano

        if self.raça.lower() == "anão":
            self.orc = {
            "nome":"","raça":"orc","classe":"","atributos":{
                "força":4,"agilidade":2,"magia":1
                }
            }
            